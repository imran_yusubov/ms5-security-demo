package az.ingress.security.configuration;

import az.ingress.security.domain.Project;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class Config {

    @Bean
    public Project getProject() {
        log.info("Creating project bean");
        return new Project(1L, "ms5-demo", "Description is Using generated security password");
    }
}
