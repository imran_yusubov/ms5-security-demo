package az.ingress.security.controller;

import az.ingress.security.domain.Project;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final Project project;

    @GetMapping("public") //no authentication
    public String sayHelloPublic(Authentication authentication, Project project) {
        return "Hello Public User " + authentication.getPrincipal() + " authorities " + authentication.getAuthorities() + " " + authentication.getDetails()
                + "Project details :" + project;
    }

    @GetMapping("user") //authenticated
    public String sayHelloAuthenticated() {
        return "Hello Authenticated User";
    }

    @GetMapping("admin") //authenticated + has role ADMIN
    public String sayHelloAdmin() {
        return "Hello Admin User";
    }

    @PostMapping("public") //no authentication
    public String postSayHelloPublic(Authentication authentication, Project project) {
        return "Hello Public User " + authentication.getPrincipal() + " authorities " + authentication.getAuthorities() + " " + authentication.getDetails()
                + "Project details :" + project;
    }

}
